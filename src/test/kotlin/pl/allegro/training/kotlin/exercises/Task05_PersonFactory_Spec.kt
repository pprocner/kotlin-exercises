package pl.allegro.training.kotlin.exercises

/*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import java.lang.reflect.Modifier
import kotlin.reflect.jvm.javaConstructor

object PersonFactorySpec : Spek({
    describe("Person.create()") {
        it("should create Person") {
            val result = Person.create("John", "Doe")

            assertEquals(result.firstName, "John")
            assertEquals(result.lastName, "Doe")
        }

        it("should not have public constructor") {
            val allConstructorsPrivate = Person::class.constructors.all {
                it.javaConstructor!!.modifiers and Modifier.PRIVATE != 0
            }

            assertTrue(allConstructorsPrivate)
        }
    }
})
*/
