package pl.allegro.training.kotlin.exercises

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.util.Random

internal class ParseDateTest {

    @Test
    fun `should parse proper date`() {
        val date = parseDate("1986-04-26")

        assertEquals(date.year, 1986)
        assertEquals(date.month, 4)
        assertEquals(date.day, 26)
    }

    @Test
    fun `should throw when date has invalid format`() {
        assertThrows(IllegalArgumentException::class.java) {
            parseDate("1986/04/26")
        }
    }

    @Test
    fun `should throw when month is out of range`() {
        assertThrows(IllegalArgumentException::class.java) {
            parseDate("1986-13-26")
        }
    }

    @Test
    fun `should throw when day is out of range`() {
        assertThrows(IllegalArgumentException::class.java) {
            parseDate("1986-04-31")
        }
    }

    @Test
    fun `should proper handle leap year`() {
        val date = parseDate("2000-02-29")

        assertEquals(date.year, 2000)
        assertEquals(date.month, 2)
        assertEquals(date.day, 29)
    }

    @Test
    fun `should work correctly for any pair of dates`() {
        repeat(10_000) {
            val date = randomDate()
            val dateStr = date.toString()
            val expectedDate = Date(date.dayOfMonth, date.monthValue, date.year)

            assertEquals(expectedDate, parseDate(dateStr))
        }
    }

    private fun randomDate(): LocalDate {
        val random = Random()
        val minDay = LocalDate.of(1900, 1, 1).toEpochDay().toInt()
        val maxDay = LocalDate.of(2100, 1, 1).toEpochDay().toInt()
        val randomDay = (minDay + random.nextInt(maxDay - minDay)).toLong()

        return LocalDate.ofEpochDay(randomDay)
    }
}