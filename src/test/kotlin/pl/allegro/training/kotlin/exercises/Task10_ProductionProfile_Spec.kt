package pl.allegro.training.kotlin.exercises
/*
import org.junit.jupiter.api.Assertions.assertEquals
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

val PRODUCTION_PROFILE = Profile(production = true, development = false, test = false)

object ProductionProfileSpec : Spek({
    describe("ProductionProfile should create production profile") {
        it("using with()") {
            assertEquals(PRODUCTION_PROFILE, ProductionProfile.usingWith())
        }

        it("using run()") {
            assertEquals(PRODUCTION_PROFILE, ProductionProfile.usingRun())
        }

        it("using let()") {
            assertEquals(PRODUCTION_PROFILE, ProductionProfile.usingLet())
        }

        it("using apply()") {
            assertEquals(PRODUCTION_PROFILE, ProductionProfile.usingApply())
        }

        it("using also()") {
            assertEquals(PRODUCTION_PROFILE, ProductionProfile.usingAlso())
        }
    }
})
*/
