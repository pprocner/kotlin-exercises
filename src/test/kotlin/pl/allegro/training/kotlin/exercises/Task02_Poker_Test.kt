package pl.allegro.training.kotlin.exercises

/*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PokerTest {

    @Test
    fun `should return royal flush`() {
        // when
        val cards = PokerHands.RoyalFlush

        // then
        assertEquals(5, cards.size)
        assertEquals(
            listOf("Ten of Spades", "Jack of Spades", "Queen of Spades", "King of Spades", "Ace of Spades").toString(),
            cards.toString()
        )
    }

    @Test
    fun `should return four of a kind`() {
        // when
        val cards = PokerHands.FourOfAKind

        // then
        assertEquals(4, cards.size)
        assertEquals(
            listOf("Jack of Hearts", "Jack of Spades", "Jack of Clubs", "Jack of Diamonds").toString(),
            cards.toString()
        )
    }

    @Test
    fun `should return straight`() {
        // when
        val cards = PokerHands.Straight

        // then
        assertEquals(5, cards.size)
        assertEquals(
            listOf(
                "Nine of Clubs", "Eight of Diamonds", "Seven of Spades", "Six of Diamonds", "Five of Hearts"
            ).toString(),
            cards.toString()
        )
    }
}
*/
