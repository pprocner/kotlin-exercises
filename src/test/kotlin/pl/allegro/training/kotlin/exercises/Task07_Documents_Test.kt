package pl.allegro.training.kotlin.exercises
/*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import java.nio.file.Files
import java.nio.file.Paths

internal class DocumentsTest {

    @Test
    fun `should read content of existing file`() {
        val content = Documents["src/test/resources/test.txt"]

        assertEquals("one two three", content)
    }

    @Test
    fun `should return null if given file does not exist`() {
        val content = Documents["non_existing.txt"]

        assertNull(content)
    }

    @Test
    fun `should save content to file`() {
        Documents["tmp.txt"] = "Hello world"

        assertEquals("Hello world", Files.readString(Paths.get("tmp.txt")))
    }

    @Test
    fun `should remove file`() {
        val name = "tmp.txt"
        val path = Paths.get(name)
        Files.writeString(path, "a b c")

        Documents[name] = null

        assertFalse(Files.exists(path))
    }
}
*/
