package pl.allegro.training.kotlin.exercises

/*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.reflect.full.memberProperties

class MyErrorTest {
    @Test
    fun `should render client side error`() {
        // given
        val error = MyError("UserNotFound", "User with id=4 not found", 404)

        // expect
        assertTrue(error.toString().startsWith("CLIENT"))
    }

    @Test
    fun `should render server side error`() {
        // given
        val error = MyError("DbConnectionFailure", "Failed to connect to db", 503)

        // expect
        assertTrue(error.toString().startsWith("SERVER"))
    }

    @Test
    fun `should throw when trying to create error with HTTP status lower than 400`() {
        assertThrows<IllegalArgumentException> {
            MyError("UserNotFound", "User with id=4 not found", 204)
        }
    }

    @Test
    fun `should render error with message`() {
        // given
        val error = MyError("UserNotFound", "User with id=4 not found", 404)

        // expect
        assertEquals("CLIENT - UserNotFound - User with id=4 not found", error.toString())
    }

    @Test
    fun `should render error without message`() {
        // given
        val error = MyError("UserNotFound", null, 404)

        // expect
        assertEquals("CLIENT - UserNotFound - <no message>", error.toString())
    }

    @Test
    fun `should create server side error when status code not given`() {
        assertEquals("SERVER", MyError("UserNotFound", "User with id=4 not found").side)
    }

    @Test
    fun `should have code, message and side properties only`() {
        val expectedProperties = listOf("code", "message", "side")
        val actualProperties = MyError::class.memberProperties.map { it.name }

        assertEquals(expectedProperties, actualProperties, "MyError has invalid properties")
    }
}
*/
