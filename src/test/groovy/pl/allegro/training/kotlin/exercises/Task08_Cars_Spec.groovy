package pl.allegro.training.kotlin.exercises

import spock.lang.Specification

class CarsSpec extends Specification {
    def "should return list of brands"() {
        expect:
        Task08_CarsKt.getBrandsOrderedAlphabetically() == ["BMW", "Fiat", "Ford"]
    }

    def "should return set of brands"() {
        expect:
        Task08_CarsKt.getBrandSet() == ["BMW", "Fiat", "Ford"] as Set
    }

    def "should return array of brands"() {
        expect:
        Task08_CarsKt.getBrandArrayOrderedAlphabetically() as List == ["BMW", "Fiat", "Ford"]
    }

    def "should return iterable of brands"() {
        expect:
        Task08_CarsKt.getBrandIterableOrderedAlphabetically() as List == ["BMW", "Fiat", "Ford"]
    }

    def "should return mutable list of brands"() {
        expect:
        Task08_CarsKt.getMutableBrandsOrderedAlphabetically() == ["BMW", "Fiat", "Ford"]
    }

    def "should return set of models"() {
        expect:
        Task08_CarsKt.getModels() == ["Fiesta", "Mondeo", "Mustang", "X5", "X6", "126p"] as Set
    }

    def "should return list of cars"() {
        expect:
        Task08_CarsKt.getCarsOrderedByBrandAndModel() as List == [
                new Car("BMW", "X5"),
                new Car("BMW", "X6"),
                new Car("Fiat", "126p"),
                new Car("Ford", "Fiesta"),
                new Car("Ford", "Mondeo"),
                new Car("Ford", "Mustang")
        ]
    }

    def "should associate cars with model"() {
        expect:
        Task08_CarsKt.getCarsAssociatedByModel() == [
                "X5"     : new Car("BMW", "X5"),
                "X6"     : new Car("BMW", "X6"),
                "126p"   : new Car("Fiat", "126p"),
                "Fiesta" : new Car("Ford", "Fiesta"),
                "Mondeo" : new Car("Ford", "Mondeo"),
                "Mustang": new Car("Ford", "Mustang")
        ]
    }

    def "should groups cars by brand"() {
        expect:
        Task08_CarsKt.getCarsGroupedByBrand() == [
                "BMW" : [new Car("BMW", "X5"), new Car("BMW", "X6")],
                "Fiat": [new Car("Fiat", "126p")],
                "Ford": [new Car("Ford", "Fiesta"), new Car("Ford", "Mondeo"), new Car("Ford", "Mustang")]
        ]
    }

    def "should return Ford's M models"() {
        expect:
        Task08_CarsKt.getFordsMModels() == ["Mondeo", "Mustang"] as Set
    }

    def "should return all models in given map"() {
        expect:
        Task08_CarsKt.getAllModels([
                "BMW" : [new Car("BMW", "X5"), new Car("BMW", "X6")],
                "Fiat": [new Car("Fiat", "126p")],
                "Ford": [
                        new Car("Ford", "Fiesta"),
                        new Car("Ford", "Mondeo"),
                        new Car("Ford", "Mustang")
                ]
        ]) == ["Fiesta", "Mondeo", "Mustang", "X5", "X6", "126p"] as Set
    }
}
