package pl.allegro.training.kotlin.exercises

data class Profile(
    val development: Boolean,
    val test: Boolean,
    val production: Boolean
)

class ProfileBuilder {
    var development: Boolean = false
    var test: Boolean = false
    var production: Boolean = false

    fun build(): Profile = Profile(development, test, production)
}

/**
 * Implement following functions. You cannot remove any existing symbols.
 */
/*
object ProductionProfile {
    fun usingWith(): Profile = with(ProfileBuilder()) {  }

    fun usingRun(): Profile = ProfileBuilder().run {  }

    fun usingLet(): Profile = ProfileBuilder().let {  }

    fun usingApply(): Profile = ProfileBuilder().apply {  }

    fun usingAlso(): Profile = ProfileBuilder().also {  }
}
*/
