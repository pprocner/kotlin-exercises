package pl.allegro.training.kotlin.exercises

import java.nio.file.Files
import java.nio.file.Paths

/**
 * Create an implementation of the `Documents` object so that the following calls should work:
 * - val text = Documents["abc.txt"] - text should contain the content of file abc.txt as a String or
 *      null if there is no such file
 *
 * - Documents["abc.txt"] = "Hello" - the abc.txt file should now contain "Hello"
 *
 * - Documents["abc.txt"] = null - the abc.txt should be removed if it exists, otherwise do nothing
 *
 * Hints:
 * - use operator overloading
 * - use Files class to do operations on file system
 */

object Documents
