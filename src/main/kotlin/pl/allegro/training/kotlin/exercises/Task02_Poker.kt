package pl.allegro.training.kotlin.exercises

/**
 * Make below DSL for defining cards compile and work correctly. You cannot change any existing code.
 */

/*
object PokerHands {
    val RoyalFlush = listOf(
        Ten of Spades,
        Jack of Spades,
        Queen of Spades,
        King of Spades,
        Ace of Spades
    )
    val FourOfAKind = listOf(
        Jack of Hearts,
        Jack of Spades,
        Jack of Clubs,
        Jack of Diamonds
    )
    val Straight = listOf(
        Nine of Clubs,
        Eight of Diamonds,
        Seven of Spades,
        Six of Diamonds,
        Five of Hearts
    )
}
*/
