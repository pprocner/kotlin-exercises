package pl.allegro.training.kotlin.exercises

/**
 * Implement all necessary functions to make it possible to have a for loop
 * over a Pair. Make [pairContains] function work correctly.
 *
 * Handy information:
 * A for loop requires an object to have an `iterator()` operator function
 * which returns an object which should have:
 * - an operator `hasNext(): Boolean` function
 * - an operator `next(): T` function
 */

/*
fun pairContains(pair: Pair<String, String>, word: String): Boolean {
    for (value in pair) {
        if (value == word) return true
    }
    return false
}
*/
