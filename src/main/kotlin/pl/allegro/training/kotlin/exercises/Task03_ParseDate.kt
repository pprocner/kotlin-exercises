package pl.allegro.training.kotlin.exercises

/**
 * Implement parseDate() function - the date will be
 * provided in yyyy-mm-dd format. You should use DATE_REGEX to parse given string.
 * Use destructuring on parsed result. If the string provided is not parsable as a proper date,
 * the function should throw [IllegalArgumentException].
 *
 * Check if any given day exceed maximum number of days in a month. If it does,
 * throw [IllegalArgumentException]. Use maksymalna_liczba_dni_w_miesiacu_danego_roku()
 * function (from Utils.kt). Make sure to provide some sane alias for it.
 *
 * Handy functions:
 * [Regex.matchEntire],
 * [MatchResult.destructured]
 * [String.toInt]
 * [maksymalna_liczba_dni_w_miesiacu_danego_roku]
 */

private val DATE_REGEX = Regex("([1-9][0-9]{3})-([0-1][0-9])-([0-3][0-9])")

data class Date(val day: Int, val month: Int, val year: Int)

fun parseDate(dateStr: String): Date = TODO()
