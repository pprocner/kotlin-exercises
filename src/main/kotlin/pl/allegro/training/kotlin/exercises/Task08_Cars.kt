package pl.allegro.training.kotlin.exercises

private val CARS = listOf("Ford Fiesta", "Ford Mondeo", "Ford Mustang", "BMW X5", "BMW X6", "Fiat 126p")

/**
 * Implement functions according to their names/descriptions. Successive functions can use previously defined ones.
 */

fun getBrandsOrderedAlphabetically(): List<String> = TODO()

fun getBrandSet(): Set<String> = TODO()

fun getBrandArrayOrderedAlphabetically(): Array<String> = TODO()

fun getBrandIterableOrderedAlphabetically(): Iterable<String> = TODO()

fun getMutableBrandsOrderedAlphabetically(): MutableList<String> = TODO()

fun getModels(): Set<String> = TODO()

fun getCarsOrderedByBrandAndModel(): List<Car> = TODO()

/** see [associateBy] */
fun getCarsAssociatedByModel(): Map<String, Car> = TODO()

/** see [groupBy] */
fun getCarsGroupedByBrand(): Map<String, List<Car>> = TODO()

/** see [flatMap] */
fun getAllModels(carsByBrands: Map<String, List<Car>>): Set<String> = TODO()

/** all Ford's models which start with M */
fun getFordsMModels(): Set<String> = TODO()

data class Car(
    val brand: String,
    val model: String
)
