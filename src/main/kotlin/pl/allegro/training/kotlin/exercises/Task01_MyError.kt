package pl.allegro.training.kotlin.exercises

/**
 * Define a final class MyError with a constructor that has the following parameters:
 * - code - type String, with no default value,
 * - message - nullable type String, with no default value,
 * - httpStatus - type Int, with the default value of 500 and minimal value
 *                of 400 (throw IllegalArgumentException if lower).
 *
 * MyError should have following read-only properties:
 * - code - having value as constructor's parameter 'code'
 * - message - having value as constructor's parameter 'message'
 * - side - having value "CLIENT" when constructor's parameter 'httpStatus' is lower than 500, otherwise "SERVER"
 *
 * MyError should not have other properties.
 *
 * MyError should have toString() method that returns string representation of object as follows:
 * - error side, code and message separated by ' - ',
 * - if message is null, toString() should render <no message> instead of message.
 *
 * Example results:
 *  CLIENT - UserNotFound - User with id=4 not found
 *  CLIENT - UserNotFound - <no message>
 */

class MyError
