package pl.allegro.training.kotlin.exercises

/**
 * Make the constructor of [Person] class private and provide a factory function
 * create(firstName: String, lastName: String): Person in companion object.
 */

class Person(
    val firstName: String,
    val lastName: String
)