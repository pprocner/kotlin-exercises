package pl.allegro.training.kotlin.exercises

/**
 * Create the following implementations of Process interface:
 * - EchoProcess<T> - the run() method should simply return the input argument,
 *
 * - ShredProcess<T> - the run() method should check if the string representation of input
 *   (i.e. the value returned by calling toString()) contains "TOP SECRET" and if it's not,
 *   it should throw [IllegalArgumentException],
 *
 * - StubProcess<T>(output: T) - the run() method should always return output given in constructor.
 */

interface Process<in I, out O> {
    fun run(input: I): O
}

class EchoProcess<T>

class ShredProcess<T>

class StubProcess<T>
